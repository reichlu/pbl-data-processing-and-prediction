# PBL Data Processing, Model Training, Prediction, Evaluation

I built the processing pipeline, then made functions for training a random forest ensemble and running cross validation. Then i let the model predict and made the evaluation notebook to analyse predictions from all models in relation to peptide sequence length. I decided to make four different files for readability.
